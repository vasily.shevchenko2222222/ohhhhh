#include <stdio.h>
#include <string>
#include "opencv2/opencv.hpp"
#include <opencv2/aruco.hpp>
#include <opencv2/highgui.hpp>

int main(int argc, char** argv )
{
    cv::Mat markerImage;
    cv::Ptr<cv::aruco::Dictionary> dictionary = cv::aruco::getPredefinedDictionary(cv::aruco::DICT_6X6_250);
    cv::aruco::drawMarker(dictionary, 22, 200, markerImage, 1);
    cv::imwrite("marker22.png", markerImage); 
    cv::aruco::drawMarker(dictionary, 21, 200, markerImage, 1);
    cv::imwrite("marker21.png", markerImage); 
    cv::aruco::drawMarker(dictionary, 20, 200, markerImage, 1);
    cv::imwrite("marker20.png", markerImage); 
    cv::aruco::drawMarker(dictionary, 19, 200, markerImage, 1);
    cv::imwrite("marker19.png", markerImage); 
    cv::aruco::drawMarker(dictionary, 18, 200, markerImage, 1);
    cv::imwrite("marker18.png", markerImage); 

    /*std::string orgImgName("markers1.jpg");
    cv::Mat inputImage = cv::imread(orgImgName);
    cv::Mat outputImage;
    inputImage.copyTo(outputImage);
    std::vector<int> markerIds;
    std::vector<std::vector<cv::Point2f>> markerCorners;
    cv::Ptr<cv::aruco::Dictionary> dictionary = cv::aruco::getPredefinedDictionary(cv::aruco::DICT_6X6_250);
    cv::aruco::detectMarkers(inputImage, dictionary, markerCorners, markerIds);

    for (int i = 0; i < markerIds.size(); ++i) {
        std::cout << markerIds[i] << " " ;
    }

    cv::aruco::drawDetectedMarkers(outputImage, markerCorners, markerIds);
    cv::imshow("out", outputImage);
    char key = (char) cv::waitKey();
    if (key == 27)
        return -1;*/

    return 0;
}
